from subprocess import call
import time
import os
import sys


base_num_points = 2**30
num_trials = 4
dry_run = False

def run_test(method, func, threads=1, processes=1):
    logdir = 'logs-{}'.format(func)
    if not os.path.exists(logdir):
        os.makedirs(logdir)
    s_num_points = str(base_num_points)
    s_num_trials = str(num_trials)
    cmd = [
            '/home_edu/edu-cmc-skpod18-320/edu-cmc-skpod18-320-19/bin/mpisubmit.pl',
                            '-t', '{}'.format(threads),
                            '-p', '{}'.format(processes),
            '--stdout', '{}/{}.{}.{}.out'.format(logdir, method, threads, processes),
            '--stderr', '{}/{}.{}.{}.dat'.format(logdir, method, threads, processes),
            'bin/{}'.format(method), func, s_num_points, s_num_trials
            ]
    if dry_run:
        print('[DRY RUN]:', ' '.join(cmd))
    else:
        print('Press Enter to run:', ' '.join(cmd))
        sys.stdin.readline()
        call(cmd)


if __name__ == '__main__':
    for func in ['C', 'M']:
        run_test('baseline', func)
        run_test('mpi', func)
        for processes in range(2, 64+1, 2):
            run_test('mpi', func, processes=processes)
        run_test('open_mp', func)
        for threads in range(2, 64+1, 2):
            run_test('open_mp', func, threads=threads)
