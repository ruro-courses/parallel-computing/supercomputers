#include <math.h>

#ifndef M_PI
#define M_PI    (3.14159265358979323846264338327950288)
#endif

#ifndef M_SQRT2
#define M_SQRT2 (1.41421356237309504880168872420969808)
#endif

// Integral of exp(sin(pi*cos(2*pi*x)))*sin(pi*cos(pi*x)) from 0 to 1
char  *monster_name  = "exp(sin(pi*cos(2pi*x))) * sin(pi*cos(pi*x))";
double monster_left  = 0;
double monster_right = 1;
double monster_value = 0;
double monster(double x)
{ return exp(sin(M_PI*cos(2*M_PI*x))) * sin(M_PI*cos(M_PI*x)); }

// Integral of 2*sqrt(1-x^2) from -1 to 1 is pi.
char  *circle_name  = "2sqrt(1-x^2)";
double circle_left  = -1;
double circle_right = 1;
double circle_value = M_PI;
double circle(double x)
{ return 2*sqrt(1 - x*x); }

// Integral of 3/4 sqrt(x) from 0 to 2 is sqrt(2).
char  *rt2_name  = "3/4sqrt(x)";
double rt2_left  = 0;
double rt2_right = 2;
double rt2_value = M_SQRT2;
double rt2(double x)
{ return 3/4 * sqrt(x); }

// Integral of x from 0 to 1 is 1/2.
char  *lin_name  = "x";
double lin_left  = 0;
double lin_right = 1;
double lin_value = .5;
double lin(double x)
{ return x; }
