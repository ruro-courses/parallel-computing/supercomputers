#include <mpi.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#if defined(LEFT)
    #define METHOD "left"
    #define SAMPLE(l, r) (l)
#elif defined(RIGHT)
    #define METHOD "right"
    #define SAMPLE(l, r) (r)
#else
    #define METHOD "middle"
    #define SAMPLE(l, r) ((l + r)/2)
#endif

double integrate(double (*f)(double), double a, double b, unsigned long long N);

#if defined(BASELINE)
    #define IMPL "baseline"
    #include "impl_baseline.c"
#elif defined(OPEN_MP)
    #define IMPL "OpenMP"
    #include "impl_open_mp.c"
#elif defined(MPI)
    #define IMPL "MPI"
    #include "impl_mpi.c"
#else
    #error Please define BASELINE, OPEN_MP or MPI implementation.
#endif

#define STR(s) #s
#define XSTR(s) STR(s)

#ifndef M_PI
#define M_PI (3.14159265358979323846264338327950288)
#endif

#include "functions.c"

int main(int argc, char *argv[])
{
    if (argc != 4)
    {
        printf("Usage: %s [function] [num_points] [num_trials]\n", argv[0]);
        return 1;
    }
    double left;
    double right;
    double target;
    char *name;
    double (*f)(double);
    switch(argv[1][0])
    {
        case 'M':
            left   = monster_left;
            right  = monster_right;
            target = monster_value;
            name   = monster_name;
            f      = monster;
            break;
        case 'C':
            left   = circle_left;
            right  = circle_right;
            target = circle_value;
            name   = circle_name;
            f      = circle;
            break;
        case 'R':
            left   = rt2_left;
            right  = rt2_right;
            target = rt2_value;
            name   = rt2_name;
            f      = rt2;
            break;
        case 'L':
            left   = lin_left;
            right  = lin_right;
            target = lin_value;
            name   = lin_name;
            f      = lin;
            break;
        default:
            printf("'%s' is not a valid function.\n", argv[1]);
            printf("Valid functions are: M, C, R and L.\n");
            return 2;
    }
    Init(&argc, &argv, f);
    unsigned long long N;
    unsigned long long trials;
    if (1 != sscanf(argv[2], "%llu", &N))
    {
        printf("%s is not a valid num_points.\n", argv[2]);
        return 3;
    }
    if (1 != sscanf(argv[3], "%llu", &trials))
    {
        printf("%s is not a valid num_trials.\n", argv[3]);
        return 4;
    }
    double times[trials];
    double errors[trials];

    printf("Evaluating method of %s rectangles with %s implementation.\n",
                               METHOD,             IMPL);
    printf("Integrating %s from %f to %f, sampling at %llu points.\n",
                       name,   left, right,            N);
    for (unsigned long long ind = 0; ind < trials; ind++)
    {
        double tstart = MPI_Wtime();
        double value = integrate(f, left, right, N);
        double tend = MPI_Wtime();
        times[ind] = tend - tstart;
        errors[ind] = fabs(value-target);
        printf("================================\n");
        printf("Computed Value: %.15g\n", value);
        printf("  Target Value: %.15g\n", target);
        printf("Absolute Error: %.15g\n", errors[ind]);
        printf("         WTime: %.15g sec\n", times[ind]);
    }

    fprintf(stderr, "[");
    for (unsigned long long ind = 0; ind < trials; ind++)
    { fprintf(stderr, "%.15g, ", times[ind]); }
    fprintf(stderr, "] # times\n");

    fprintf(stderr, "[");
    for (unsigned long long ind = 0; ind < trials; ind++)
    { fprintf(stderr, "%.15g, ", errors[ind]); }
    fprintf(stderr, "] # errors\n");

    Finalize();
    return 0;
}
