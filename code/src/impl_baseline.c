#ifndef SAMPLE
    #error This file can only be compiled with main.c, read README.md for more info.
#endif

#define Init(argc, argv, f) MPI_Init(argc, argv);   // Doesn't need initialization.
#define Finalize() MPI_Finalize();                  // Doesn't need finalization.

double integrate(double (*f)(double), double a, double b, unsigned long long N)
{
    if (a > b) { return -integrate(f, b, a, N); }

    double cumsum = 0;
    double step = (b-a)/N;
    double last = a;
    double next;
    for (unsigned long long ind = 1; ind <= N; ind++)
    {
        next = a + step*ind;
        cumsum += f(SAMPLE(last, next));
        last = next;
    }
    return step * cumsum;
}
