#ifndef SAMPLE
    #error This file can only be compiled with main.c, read README.md for more info.
#endif

#define CHECK_OK(X) if (MPI_SUCCESS != (X))             \
                    {                                   \
                        printf("MPI error.\n");         \
                        MPI_Abort(MPI_COMM_WORLD, -1);  \
                        exit(-1);                       \
                    }
#define ROOT_RANK 0

int _comm_rank = 0;
int _comm_size = 1;

void Finalize()
{
    if (ROOT_RANK == _comm_rank)
    {
        // Send stop signal to all looping slaves.
        unsigned long long stop = 0;
        CHECK_OK(MPI_Bcast(&stop, 1, MPI_UNSIGNED_LONG_LONG,
                            ROOT_RANK, MPI_COMM_WORLD));
    }
    // Wait for slaves to also stop.
    CHECK_OK(MPI_Barrier(MPI_COMM_WORLD));
    // Finalize together.
    CHECK_OK(MPI_Finalize());
}

double _sum(double (*f)(double), double a, double b, unsigned long long N)
{
    unsigned long long subN = (N + _comm_size - 1)/_comm_size;
    double segment = (b-a)/_comm_size;
    double subA = a + _comm_rank * segment;
    double subStep = segment/subN;

    double cumsum = 0;
#ifdef MPI_OPENMP
    #pragma omp parallel for reduction(+: cumsum)
#endif
    for (unsigned long long ind = 0; ind < subN; ind++)
    { cumsum += f(SAMPLE(subA + subStep*ind, subA + subStep*(ind+1))); }
    return cumsum;
}

void slaveloop(double (*f)(double))
{
    while (1)
    {
        // Get number of points to integrate over.
        unsigned long long N;
        CHECK_OK(MPI_Bcast(&N, 1, MPI_UNSIGNED_LONG_LONG,
                            ROOT_RANK, MPI_COMM_WORLD));
        if (0 == N) { break; } // Recieved stop signal.

        // Get global integration bounds.
        double ab[2];
        CHECK_OK(MPI_Bcast(ab, 2, MPI_DOUBLE, ROOT_RANK, MPI_COMM_WORLD));

        // Integrate
        double a = ab[0], b = ab[1];
        double cumsum = _sum(f, a, b, N);

        // Send results and wait for everybody to finish.
        CHECK_OK(MPI_Reduce(&cumsum, NULL, 1, MPI_DOUBLE, MPI_SUM,
                            ROOT_RANK, MPI_COMM_WORLD));
    }
    Finalize();
    exit(0);
}

void Init(int *argc, char **argv[], double (*f)(double))
{
    CHECK_OK(MPI_Init(argc, argv));
    CHECK_OK(MPI_Comm_size(MPI_COMM_WORLD, &_comm_size));
    CHECK_OK(MPI_Comm_rank(MPI_COMM_WORLD, &_comm_rank));
    // Root process returns from Init, other processes wait for tasks.
    if (ROOT_RANK == _comm_rank) { return; }
    else { slaveloop(f); }
}

double integrate(double (*f)(double), double a, double b, unsigned long long N)
{
    if (a > b) { return -integrate(f, b, a, N); }

    double ab[2] = {a, b};
    CHECK_OK(MPI_Bcast(&N, 1, MPI_UNSIGNED_LONG_LONG, ROOT_RANK, MPI_COMM_WORLD));
    CHECK_OK(MPI_Bcast(ab, 2, MPI_DOUBLE, ROOT_RANK, MPI_COMM_WORLD));

    double cumsum = _sum(f, a, b, N);
    CHECK_OK(MPI_Reduce(MPI_IN_PLACE, &cumsum, 1, MPI_DOUBLE, MPI_SUM,
                        ROOT_RANK, MPI_COMM_WORLD));
    return cumsum*(b-a)/N;
}
