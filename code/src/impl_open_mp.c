#ifndef SAMPLE
    #error This file can only be compiled with main.c, read README.md for more info.
#endif

#define Init(argc, argv, f) MPI_Init(argc, argv);   // Doesn't need initialization.
#define Finalize() MPI_Finalize();                  // Doesn't need finalization.

double integrate(double (*f)(double), double a, double b, unsigned long long N)
{
    if (a > b) { return -integrate(f, b, a, N); }

    double cumsum = 0;
    double step = (b-a)/N;
    #pragma omp parallel for reduction(+: cumsum)
    for (unsigned long long ind = 0; ind < N; ind++)
    { cumsum += f(SAMPLE(a + step*ind, a + step*(ind+1))); }
    return step * cumsum;
}
