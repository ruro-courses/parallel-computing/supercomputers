import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.font_manager as fonts
import matplotlib as mpl
import numpy as np
import glob
import os
import sys

mpl.rc('axes', axisbelow=True)
monospace_font = fonts.FontProperties()
monospace_font.set_family('monospace')

def load_stats(log_dir, methods):
    stats = {}
    for method in methods:
        logs = glob.glob(os.path.join(log_dir, method+'.*.dat'))
        results = []
        errors = []
        sizes = []
        for ind, log in enumerate(logs):
            result = []
            error = []
            with open(log) as logfile:
                while True:
                    resline = logfile.readline()
                    errline = logfile.readline()
                    if resline == '' or errline == '':
                        break
                    try:
                        result.extend(eval(resline))
                        error.extend(eval(errline))
                    except Exception as e:
                        print(log, e)
            if len(result) == 0 or len(error) == 0:
                print(log, "No timings")
            results.append(np.asarray(result))
            errors.append(np.asarray(error))
            params = log.split('.')
            sizes.append(int(params[-2]) * int(params[-3]))
        results = np.asarray(results)
        errors = np.asarray(errors)
        sizes = np.asarray(sizes)
        reorder = np.argsort(sizes)
        errors = errors[reorder]
        results = results[reorder]
        sizes = sizes[reorder]

        stats[method] = sizes, results, errors
    return stats

from numpy import sqrt, sin, cos, pi, exp
x = np.linspace(0, 1, 1000)
monster_dat = [x, exp(sin(pi*cos(2*pi*x))) * sin(pi*cos(pi*x))]
x = np.linspace(-1, 1, 1000)
circle_dat = [x, sqrt(1-x*x), -sqrt(1-x*x)]
x = np.linspace(0, 2, 1000)
sqrt_dat = [x, 3/4*sqrt(x)]
x = np.linspace(0, 1, 1000)
lin_dat = [x, x]

functions = {
        'M':('The Monster', monster_dat),
        'C':('Circle Area', circle_dat),
        #'R':('Square Root', sqrt_dat),
        #'L':('Linear', lin_dat)
}

pimpls = [
    ('open_mp', 'OpenMP', 'C2', '#86de86'),
    ('mpi', 'MPI', 'C3', '#eb9292'),
]

bimpls = [
    #('open_mp', 'OpenMP', 'C2', '#86de86'),
    ('mpi', 'MPI', 'C3', '#eb9292'),
    ('mpi_open_mp.2', 'MPI+OpenMP (2 Threads)', 'C4', '#c9b3de'),
    ('mpi_open_mp.4', 'MPI+OpenMP (4 Threads)', 'C5', '#cba69e'),
]

p_data_stats = {
    func_id:load_stats('logs-P{}'.format(func_id),
        ['baseline', 'open_mp', 'mpi'])
    for func_id in functions.keys()
}

b_data_stats = {
    func_id:load_stats('logs-B{}'.format(func_id),
        ['baseline', 'mpi', 'mpi_open_mp.2', 'mpi_open_mp.4'])
    for func_id in functions.keys()
}

def mkplot_alpha(impls, data_stats):
    fig, ax = plt.subplots(2, 1, figsize=(9.6, 1.2*len(impls)),
            gridspec_kw = {'height_ratios':[6, 4]})
    #ax[0].set_adjustable('box')
    for ind, spine in ax[0].spines.items():
        if ind != 'top':
            spine.set_visible(False)
    #ax[1].set_adjustable('box')
    for ind, spine in ax[1].spines.items():
        if ind != 'top':
            spine.set_visible(False)
    
    alphas_max = []
    alphas_std = []
    labels = []
    for func_id in functions.keys():
        stats = data_stats[func_id]

        func_name, _ = functions[func_id]
        for impl, source_name, impl_name, _, _ in impls:
            baseline_sz, baseline_times, baseline_err = stats[impl[0]+'baseline']
            baseline = np.min(baseline_times)

            sz, times, err = stats[impl]
            sz, times, err = sz[1:][:], times[1:], err[1:]
            szs = sz[:, np.newaxis]
            alpha_all = np.clip(
                    (szs - baseline/times)/
                    (baseline/times*(szs - 1)),
                    np.nan, None)
            alpha_all = alpha_all[3*alpha_all.shape[0]//4:]
            alpha = np.min(alpha_all)
            alpha_std = np.mean((alpha-alpha_all)**2)
            alphas_max.append(alpha)
            alphas_std.append(alpha_std)
            labels.append("{:^8} | {:^22} | {:^11}".format(
                        source_name, impl_name, func_name))

    order = sorted(range(len(labels)), key=labels.__getitem__)
    labels = sorted(labels)
    alphas_max = np.asarray(alphas_max)[order]
    alphas_std = np.asarray(alphas_std)[order]
    alphas_std = np.stack([alphas_std, np.zeros_like(alphas_std)], axis=0)

    def plotbars(ax, alphas_max, alphas_std, labels, lim, ticks):
        ax.barh(np.arange(len(alphas_max)), 1-alphas_max, xerr=alphas_std,
                height=.9, label='Параллелизуемый код')
        ax.barh(np.arange(len(alphas_max)), alphas_max, left=1-alphas_max,
                height=.9, label='Не параллелизуемый код')
        ax.set_yticks(np.arange(len(alphas_max)))
        ax.set_yticklabels(labels, fontProperties=monospace_font)
        ax.set_xlim(lim, 1)
        ax.set_xticks(np.linspace(lim, 1, ticks))
        ax.set_xticks(np.linspace(lim, 1, 2*ticks - 1), minor=True)
        ax.tick_params(top=True, bottom=False, left=False, right=False,
                labeltop=True, labelbottom=False, labelleft=True)
        ax.tick_params(which='minor', bottom=False, top=True)

    plotbars(ax[1], alphas_max[:4], alphas_std[:, :4], labels[:4], 0.91, 10)
    plotbars(ax[0], alphas_max[4:], alphas_std[:, 4:], labels[4:], 0.9995, 6)

    ax[1].legend(loc='upper center', bbox_to_anchor=(.5, 0))

    return fig


def mkplots_eff(func_id, impls, data_stats):
    stats = data_stats[func_id]
    baseline_sz, baseline_times, baseline_err = stats['baseline']
    baseline = np.min(baseline_times)
    sz_all = set([size for sizes, _, _ in stats.values() for size in sizes])
    sz_all = sorted(list(sz_all))

    fig, ax = plt.subplots(1, 1, figsize=(9.6, 9.6))

    ax.grid()
    ax.set_aspect(1)
    ax.set_adjustable('box')
    ax.axhline(1, linestyle='--', label='Baseline', color='C0')
    ax.plot(sz_all, sz_all, linestyle='--',
            label='Theoretical maximum', color='C1')

    for impl, name, edgecolor, facecolor in impls:
        sz, times, err = stats[impl]
        speedup = baseline/np.min(times, axis=-1)
        speedup_err = np.mean(
                np.abs(speedup[:, np.newaxis] - baseline/times),
                axis=-1)

        ax.fill_between(sz, speedup, speedup-speedup_err,
                alpha=.5, linestyle=':',
                edgecolor=edgecolor, facecolor=facecolor)
        ax.plot(sz, speedup, label=name, color=edgecolor)

    ax.legend()
    ax.set_xlabel('Number of Compute Units*')
    ax.set_ylabel('Speedup')

    func_name, _ = functions[func_id]
    return fig


def mkplots_fun(func_id):
    fig, ax = plt.subplots(1, 1, figsize=(9.6, 9.6))

    ax.set_adjustable('box')
    func_name, func_data = functions[func_id]
    if len(func_data) == 2:
        x, f = func_data
        ax.fill_between(x, np.maximum(f, 0), 0,
                edgecolor='black', facecolor='C0')
        ax.fill_between(x, 0, np.minimum(f, 0),
                edgecolor='black', facecolor='C3')
        ax.set_aspect((np.max(x)-np.min(x))/(np.max(f)-np.min(f)))
    else:
        x, f0, f1 = func_data
        ax.fill_between(x, f0, f1, edgecolor='black', facecolor='C0')
        ax.set_aspect((np.max(x)-np.min(x))/(np.max(f0)-np.min(f1)))

    ax.grid()
    ax.set_xlabel('x')
    ax.set_ylabel('f(x)')
    return fig


for func_id in functions.keys():
    mkplots_eff(func_id, pimpls, p_data_stats).savefig(
            'images/eff_figP{}.png'.format(func_id), bbox_inches='tight', dpi=200)
    mkplots_eff(func_id, bimpls, b_data_stats).savefig(
            'images/eff_figB{}.png'.format(func_id), bbox_inches='tight', dpi=200)
    mkplots_fun(func_id).savefig(
            'images/fun_fig{}.png'.format(func_id), bbox_inches='tight', dpi=200)

all_impls = []
all_impls.extend(('p'+impl, "Polus" , impl_name, c1, c2)
                    for impl, impl_name, c1, c2 in pimpls)
all_impls.extend(('b'+impl, "Bluegene" , impl_name, c1, c2)
                    for impl, impl_name, c1, c2 in bimpls)

all_data_stats = {func_id:{} for func_id in functions.keys()}
for func, data in p_data_stats.items():
    for impl, stat in data.items():
        all_data_stats[func]['p'+impl] = stat
for func, data in b_data_stats.items():
    for impl, stat in data.items():
        all_data_stats[func]['b'+impl] = stat

mkplot_alpha(all_impls, all_data_stats).savefig('images/alpha_fig.png',
                                                    bbox_inches='tight', dpi=200)
