CC=mpicc
CFLAGS+= -std=c99 -Wall -O3 -fopenmp -lm

bin/%: src/main.c src/functions.c src/impl_%.c
	@echo Compiling $(shell basename $@) implementation.
	mkdir -p $(shell dirname $@)
	$(CC) $< -o $@ $(CFLAGS) -D$(shell basename $@ | tr 'a-z' 'A-Z')

.PHONY: all baseline open_mp mpi clean
baseline: bin/baseline
open_mp: bin/open_mp
mpi: bin/mpi
all: baseline open_mp mpi
clean:
	rm -rf bin
