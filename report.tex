%&XeTeX
% !TEX encoding = UTF-8 Unicode
\documentclass[a4paper, 12pt]{article}
\usepackage[a4paper, left=15mm, right=15mm, top=20mm, bottom=20mm]{geometry}
\usepackage{fontspec}
\defaultfontfeatures{Ligatures=TeX}
\setmainfont{CMU Serif}
\setmonofont{CMU Typewriter Text}
\setsansfont{CMU Sans Serif}
\usepackage{hyperref}
\usepackage{hyphenat}
\usepackage[english, russian]{babel}
\usepackage{subfig}
\usepackage{float}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{relsize}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{indentfirst}
\lstset{escapeinside={(*@}{@*)}, frame=single, showstringspaces=false, columns=fixed, basicstyle={\footnotesize\ttfamily}, commentstyle={\it}, keywordstyle={\textbf}, numbers=left, tabsize=4, caption={\texttt{\lstname}}}
\begin{document}
\begin{titlepage}
    \centering\noindent
    {
        \begin{minipage}{0.1\textwidth}
            \includegraphics[width=\textwidth]{images/logo-mgu.png}
        \end{minipage}
        \hfill
        \begin{minipage}{0.77\textwidth}
            \begin{center}
                \textbf{МОСКОВСКИЙ ГОСУДАРСТВЕННЫЙ УНИВЕРСИТЕТ}\par
                \textbf{имени М.В.Ломоносова}\par
            \end{center}
        \end{minipage}
        \hfill
        \begin{minipage}{0.1\textwidth}
            \includegraphics[width=\textwidth]{images/logo-cmc.png}
        \end{minipage}
    }
    \par
    {
        \textbf{Факультет вычислительной математики и кибернетики}\par
        \nointerlineskip
        \noindent\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}
    }
    \vfill
    {
        \Large{\textbf{Компьютерный практикум по учебному курсу}}\par
        \Large{\textbf{«СУПЕРКОМПЬЮТЕРЫ И ПАРАЛЛЕЛЬНАЯ ОБРАБОТКА ДАННЫХ»}}\par
    }
    \vfill
    {
        \Huge{\textbf{ЗАДАНИЕ}}\par
        \Large{Разработка параллельной версии программы для вычисления определенного интеграла с использованием метода прямоугольников}\par
    }
    \vfill
    {
        \Large{\textbf{ОТЧЕТ}}\par
        \Large{\textbf{о выполненном задании}}\par
        \Large{студента 320 учебной группы факультета ВМК МГУ}\par
        \Large{Стоцкого Андрея Владимировича}\par
    }
    \vfill
    {\Large Москва, \today}
\end{titlepage}
\tableofcontents
\newpage
\section{Постановка задачи}
\noindent Требуется реализовать программу для вычисления определенного интеграла функции одной переменной методом прямоугольников.
Метод прямоугольников --- метод численного интегрирования, заключающийся в замене подынтегральной функции на многочлен
нулевой степени, то есть константу, на каждом элементарном отрезке.
\vspace{\baselineskip}

\noindent Пусть есть функция $f{:}\, \left[a;\;b\right] \to \mathbb{R}$ тогда метод прямоугольников приближает значение интеграла как:
\begin{align*}
    \int\limits_{a}^{b}f\left(x\right)\,\mathrm{d}x &\;\approx\; h \sum\limits_{i=0}^{N-1} f\left(x_{i}\right)  \\\\
    x_{i} &\;=\; \begin{cases}
        a + i\mathrm{h}                         &\text{для метода \textit{левых} прямоугольников}   \\
        a + \mathrm{h} + i\mathrm{h}            &\text{для метода \textit{правых} прямоугольников}  \\
        a + \frac{\mathrm{h}}{2} + i\mathrm{h}  &\text{для метода \textit{средних} прямоугольников} \\
            \end{cases}                 \\\\
    \mathrm{h} &\;=\; \frac{b-a}{N}     \\
\end{align*}~

\begin{figure}[!hb]
    \centering
    \subfloat[Левых]{{\includegraphics[width=0.3\textwidth]{images/integral_left.png}}}%
    \qquad
    \subfloat[Правых]{{\includegraphics[width=0.3\textwidth]{images/integral_right.png}}}%
    \qquad
    \subfloat[Средних]{{\includegraphics[width=0.3\textwidth]{images/integral_middle.png}}}%
    \caption{Методы левых, правых и средних прямоугольников}
\end{figure}~

\noindent Затем требуется:
\begin{itemize}[noitemsep, topsep=0pt, itemindent=*]
    \item реализовать оптимизированную версию программы, используя технологии OpenMP и MPI
    \item протестировать оптимизированные реализации на суперкомпьютере Polus и Bluegene
    \item исследовать масштабируемость каждой реализации
    \item сравнить эффективность оптимизированных версий программы
\end{itemize}~
\newpage

\section{Реализации}
\noindent Код проекта, вспомогательных скриптов и отчета доступны на \href{https://github.com/RuRo/sc_pdp}{github.com/RuRo/sc\_pdp}.
\vspace{\baselineskip}

\noindent Было реализовано 4 версии алгоритма:
\begin{itemize}[noitemsep, topsep=0pt, itemindent=*]
    \item {baseline} -- базовый метод, без параллелизма (см. \hyperref[sec:baseline]{impl\_baseline.c})
    \item {OpenMP} -- оптимизация с использованием thread-параллелизма (см. \hyperref[sec:openmp]{impl\_open\_mp.c})
    \item {MPI} -- оптимизация с использованием процессорного параллелизма (см. \hyperref[sec:mpi]{impl\_mpi.c})
    \item {MPI+OpenMP} -- оптимизация с использованием обоих вариантов параллелизма
\end{itemize}

\subsection{OpenMP}
\noindent Для OpenMP реализации использовались директивы parallel, for и reduction.
\begin{itemize}[noitemsep, topsep=0pt, itemindent=*]
    \item {parallel} -- для создания нитей,
    \item {for} -- для распределения итераций цикла и
    \item {reduction} -- для редукции суммы.
\end{itemize}
\noindent Вместо директив private/shared использовались имплицитные scope правила.

\subsection{MPI}
\noindent Для MPI версии использовались команды MPI\_Bcast, MPI\_Reduce и MPI\_Barrier.
\begin{itemize}[noitemsep, topsep=0pt, itemindent=*]
    \item {MPI\_Bcast} -- для рассылки количества точек и пределов интегрирования,
    \item {MPI\_Reduce} -- для редукции результатов интегрирования и
    \item {MPI\_Barrier} -- для синхронизации работы процессов.
\end{itemize}
\noindent Каждая MPI операция обернута в макрос CHECK\_OK, гарантирующий, что операция завершилась без ошибок.

\subsection{MPI+OpenMP}
\noindent Версия MPI+OpenMP создана на основе MPI версии, добавлением в рабочий цикл
директив parallel, for и reduction, описанных в параграфе про OpenMP.
Для компиляции данной версии требуется установить флаг MPI\_OPENMP.

\newpage
\section{Тестирование}
\subsection{Функции}
\noindent В процессе тестирования использовалось 4 различных подынтегральных функции.
\begin{align*}
    \text{Linear:\qquad}      &\qquad f{\left(x\right)} = x \\
    \text{Square Root:\qquad} &\qquad f{\left(x\right)} = \frac{3}{4}\sqrt{x} \\
    \text{Circle Area:\qquad} &\qquad f{\left(x\right)} = 2\sqrt{1-x^2} \\
    \text{The Monster:\qquad} &\qquad f{\left(x\right)} = e^{\sin{\left(\pi\cos{2\pi x}\right)}}\sin{\left(\pi\cos{ \pi x}\right)} \\
\end{align*}
\begin{figure}[!hb]
    \centering
    \subfloat[Linear]{{\includegraphics[width=0.425\textwidth]{images/fun_figL.png}}}%
    \qquad
    \subfloat[Square Root]{{\includegraphics[width=0.425\textwidth]{images/fun_figR.png}}}%
    \\
    \subfloat[Circle Area]{{\includegraphics[width=0.425\textwidth]{images/fun_figC.png}}}%
    \qquad
    \subfloat[The Monster]{{\includegraphics[width=0.425\textwidth]{images/fun_figM.png}}}%
    \caption{Области, соответствующие вычисляемым интегралам}
\end{figure}
\newpage
\subsection{Сравнение эффективности}
\noindent Тестирование проводилось на двух суперкомпьютерах -- Polus и Bluegene.
Для замера времени использовалась функция MPI\_Wtime, и за время выполнения
принималось все время, начиная с вызова функции-интегратора до возврата значения интеграла.
На графиках по вертикали множитель ускорения по сравнению с baseline методом,
по горизонтали - количество вычислительных юнитов.
(Количество нитей для OpenMP, процессов для MPI и общее количество нитей во всех процессах для MPI+OpenMP)
\vspace{\baselineskip}

\noindent В процессе тестирования выяснилось, что функции Linear, Square Root и Circle Area одинаковы по ускорению при
оптимизации уровня $-O3$ или выше. В связи с этим графики ускорения для функций Linear и Square Root были удалены.
\begin{figure}[!ht]
    \centering
    \subfloat[Circle Area]{{\includegraphics[width=0.5\textwidth]{images/eff_figPC.png}}}%
    \subfloat[The Monster]{{\includegraphics[width=0.5\textwidth]{images/eff_figPM.png}}}%
    \caption{Polus}
\end{figure}

\noindent На суперкомпьютере Polus каждая функция была проинтегрирована в $2^{30}$ точках.
Это число было подобрано таким образом, чтобы один запуск baseline программы занимал меньше 15 минут.
\vspace{\baselineskip}

\noindent OpenMP/MPI версии были протестированы на различных количествах вычислительных юнитов от 2 до 64 с шагом 2.
\newpage
\begin{figure}[!hb]
    \subfloat[Circle Area]{{\includegraphics[width=0.5\textwidth]{images/eff_figBC.png}}}%
    \subfloat[The Monster]{{\includegraphics[width=0.5\textwidth]{images/eff_figBM.png}}}%
    \caption{Bluegene}
\end{figure}
\noindent На суперкомпьютере Bluegene каждая функция была проинтегрирована в $2^{26}$ точках.
Данное отличие связано с тем, что базовая скорость Bluegene ниже, чем у суперкомпьютера Polus в $\approx 8$ раз.
\vspace{\baselineskip}

\noindent К сожалению, архитектура суперкомпьютера Bluegene не позволяет создание более 4 нитей на 1 процесс.
В связи с этим, вместо OpenMP версии на суперкомпьютере Bluegene была протестирована MPI+OpenMP версия,
в которой для каждого процесса MPI создавалось 2 или 4 нити.
\vspace{\baselineskip}

\noindent MPI версия была протестирована на различных количествах процессов с шагом 2 от 2 до 64 и далее с шагом 64 от 64 до 512.
MPI+OpenMP версии были протестированы на таком же количестве процессов, кроме тех случаев, когда суммарное количество
вычислительных юнитов превышало 512.
\vspace{\baselineskip}

\noindent Любопытно, что ускорение MPI версии оказалось выше, чем OpenMP версии. Скорее всего это связано с тем, что
при использовании многопоточности некоторые ресурсы физического ядра оказываются разделенными между виртуальными ядрами.
При стандартном распределении MPI процессов на суперкомпьютерах такого не происходит. К тому же, основной плюс OpenMP
-- возможность использования общей памяти -- не актуален для задачи вычисления определенного интеграла функции одной
переменной методом прямоугольников.
\vspace{\baselineskip}

\noindent Также стоит отметить высокое количество выбросов на Polus, по сравнению с Bluegene. Предположительно, это связано с
особенностями планировщиков Polus и Bluegene. Эмпирически, мной было установлено, что количество выбросов на Polus
увеличивается, когда множество пользователей используют суперкомпьютер одновременно.
\newpage
\subsection{Анализ}
\begin{align*}
    S_{p} &= \frac{1}{\alpha+\frac{1-\alpha}{p}} \\ &\\
    \alpha &= \frac{p-S_{p}}{S_{p}\left(p-1\right)} \qquad \left(p > 1\right)
\end{align*}
\noindent Существует закон Амдала, который позволяет вычислить коэффициент ускорения программы $S_{p}$,
исходя из количества процессов $p$, и доли программы, \textit{не} допускающей параллелизацию $\alpha$.
Тогда для случаев, когда $p > 1$ мы можем выразить $\alpha$. Сравним $\alpha$ для различных
реализаций программы и различных функций.
\begin{figure}[!hb]
    \centering
    \includegraphics[width=\textwidth]{images/alpha_fig.png}
    \caption{Сравнение $\alpha$ для разных функций и реализаций}
\end{figure}
\subsection{Проверка корректности}
\noindent Каждый тест проводился 4-8 раз для избавления от случайных выбросов.
Так как для детерминированной процедуры невозможно случайное ускорение работы,
а только лишь случайное замедление, на графиках в каждой точке изображен
минимум времени работы программы. Для оценки размеров выбросов, на графиках
также отмечены средние отклонения выбросов от минимума.
\vspace{\baselineskip}

\noindent Интегрируемые функции и границы интегрирования были подобраны так, чтобы значения интегралов совпадали с
широко известными константами. Помимо скорости работы, также рассматривалась разница между эталонным значением
константы и вычисленным значением интеграла. Исходя из значения ошибки было принято решение, что алгоритмы работают
корректно.
\vspace{\baselineskip}

\noindent Любопытно, что для параллельных версий точность даже выше, чем в baseline версии. Я предполагаю, что это связано
с особенностями чисел с плавающей точкой. Для таких чисел, добавляя малое число $\varepsilon_i$ к уже посчитанной части
интегральной суммы $\Sigma$, зачастую $\varepsilon_i \ll \Sigma$, что приводит к усечению $\varepsilon_i$ и потере точности.
В параллельных версиях программы $\varepsilon_i$ сначала суммируются между собой в каждом процессе, и только потом эти
подсуммы добавляются в $\Sigma$, что уменьшает ошибку усечения.
\section{Листинг кода программы}
\lstset{caption={}}
\begin{lstlisting}
code
(*@├──@*) Makefile.bg
(*@├──@*) Makefile.pl
(*@├──@*) plot_stats.py
(*@├──@*) run_tests.bg.py
(*@├──@*) run_tests.pl.py
(*@└──@*) src
    (*@├──@*) functions.c
    (*@├──@*) impl_baseline.c
    (*@├──@*) impl_mpi.c
    (*@├──@*) impl_open_mp.c
    (*@└──@*) main.c
\end{lstlisting}

\subsection{\texttt{code/}}
\lstset{caption={\texttt{\lstname}}}
\lstinputlisting[language={[gnu]make}]{code/Makefile.bg}
\lstinputlisting[language={[gnu]make}]{code/Makefile.pl}
\lstinputlisting[language={Python}]{code/plot_stats.py}
\lstinputlisting[language={Python}]{code/run_tests.bg.py}
\lstinputlisting[language={Python}]{code/run_tests.pl.py}

\subsection{\texttt{code/src}}
\lstinputlisting[language={[ANSI]C}]{code/src/functions.c}
\label{sec:baseline}
\lstinputlisting[language={[ANSI]C}]{code/src/impl_baseline.c}
\label{sec:mpi}
\lstinputlisting[language={[ANSI]C}]{code/src/impl_mpi.c}
\label{sec:openmp}
\lstinputlisting[language={[ANSI]C}]{code/src/impl_open_mp.c}
\lstinputlisting[language={[ANSI]C}]{code/src/main.c}

\end{document}
